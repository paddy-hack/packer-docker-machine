Packer Docker Machine License
=============================

As a matter of policy, all "code" should be released under the terms of
the GNU General Public License (GPL), version 3 or (at your option) any
later version.  You can find a copy of this license at:

  https://www.gnu.org/licenses/gpl.html

All files should contain a line near the top saying something like

```
License: GPL-3.0+
```

to indicate this.
