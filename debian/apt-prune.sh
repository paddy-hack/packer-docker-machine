#!/bin/sh -eux
# apt-prune.sh -- purge all non-necessary packages
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

# This script is meant to be run immediately after installation of the
# machine.

# Clear the list of "manually" installed packages.
apt-mark auto $(apt-mark showmanual)

# These were explicitly added during initial system installation.  See
# the pkgsel/include setting in preseed.cfg.
apt-mark manual \
         openssh-server \
         sudo \
         # end-of-list

# Make sure to keep basic network functionality and text editors that
# will keep both sides in the Text Editor Wars happy.
apt-mark manual \
         ifupdown \
         isc-dhcp-client \
         nano \
         vim-tiny \
         wget \
         # end-of-list

# Keep system logs for trouble shooting purposes and a generic kernel
# image to ease upgrading of machines in case they cannot be replaced
# immediately.
apt-mark manual \
         linux-image-amd64 \
         logrotate \
         rsyslog \
         # end-of-list

apt-get --purge autoremove --assume-yes
