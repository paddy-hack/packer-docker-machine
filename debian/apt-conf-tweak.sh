#!/bin/sh -eux
# apt-conf-tweak.sh -- things to taste a little bit
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

# Minimize the list of sources to download (and clean up).
sed -i \
    -e '/deb-src/d' \
    -e '/^# /d' \
    -e '/^[ \t]*$/d' \
    /etc/apt/sources.list
