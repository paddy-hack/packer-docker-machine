#!/bin/sh -eux
# cleanup.sh -- remove unnecessary files
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

# Do not keep bulky *.deb's and package information files lying around.
find \
    /var/cache/apt/ \
    /var/lib/apt/ \
    -type f -delete

# Never mind backups.
find / -name '*~' -delete
find /var -name '*-old' -delete

# Wipe disk to improve image compression.
dd if=/dev/zero of=/tmp/zero || rm /tmp/zero
