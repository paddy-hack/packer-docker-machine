#!/bin/sh -eux
# install-compose.sh -- from the upstream GitHub releases
# Copyright (C) 2017, 2019  Olaf Meeuwissen
#
# License: GPL-3.0+

VERSION=1.24.0
TARGET=/usr/local/bin/docker-compose
HREF=https://github.com/docker/compose/releases/download

wget -q -O ${TARGET} ${HREF}/${VERSION}/docker-compose-$(uname -s)-$(uname -m)
chmod +x ${TARGET}
